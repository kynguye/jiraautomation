package cases;

import com.autobots.seleniumintegration.util.SeleniumDriverFactory;
import com.autobots.core.webdriver.AutoWebDriver;
import com.autobots.core.webdriver.WebDriverFactory;
import com.autobots.testng.AbstractBaseTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import util.ResolverProfile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by DellN4010 on 1/6/2015.
 */

@ContextConfiguration(locations = "/profiles.xml")
@ActiveProfiles(resolver = ResolverProfile.class)
public abstract class BaseTest extends AbstractBaseTest {
    protected AutoWebDriver driver;

    @Resource(name = "baseUrl")
    protected String url;

    @BeforeClass
    @Parameters({"platform", "browser"})
    public void baseSetup(@Optional String platform, @Optional String browser) throws IOException {
        logger.info("Current Thread "+Thread.currentThread().getId() + "Browser: " + browser);
        driver = SeleniumDriverFactory.createDriverMatchWith("config.properties", browser, platform);
        WebDriverFactory.setAutoWebDriver(driver);
        driver.openBrowser(url);
        driver.maximizeBrowserWindow();
    }

    @AfterClass
    public void baseTearDown(){
        driver.closeAllBrowsers();
    }
}
