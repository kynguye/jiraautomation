package cases;

import com.autobots.core.webcontrol.Label;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.*;


import javax.annotation.Resource;


/**
 * Created by DellN4010 on 1/6/2015.
 */

public class Demo_JIRA_Test extends BaseTest {

    private int timeout = 5000;
    @Resource(name="HomePage")
    private HomePage homePage ;

    @Resource(name="LoginPage")
    private LoginPage loginPage ;

    @Resource(name="CreateIssuePage")
    private CreateIssuePage createIssuePage ;

    @Resource(name="SearchResultPage")
    private SearchResultPage SearchResultPage ;

    @Resource(name="EditIssuePage")
    private EditIssuePage EditIssuePage ;



     @BeforeClass
    public void setup() throws InterruptedException{
        logger.info("In Setup");
         loginPage.login("nguyenchauky@gmail.com", "123456789");
       }

    @AfterClass
    public void tearDown() throws InterruptedException {
        logger.info("in TearDown");

    }

    @Test
    public void CreateNewIssue() throws InterruptedException{
        logger.info("New issues can be created");
        //For Demo test I just create an simple issue, all other fields are default)
        createIssuePage.SimpleCreateIssue("This is the test by KyNguyen");
        createIssuePage.issueCreated.waitForExist(timeout);
        verifyTrue(createIssuePage.issueCreated.isVisible());
    }

    @Test (dependsOnMethods = "CreateNewIssue")
    public void SearchAnIssue() throws InterruptedException{
        logger.info("Existing Issue can be searched");
        homePage.searchTextBox.waitForExist(timeout);
        //For demo test please accept the Issue is existed.
        homePage.searchTextBox.enterText("TST-60668\n");
        SearchResultPage.editButton.waitForExist(timeout);
        verifyTrue(SearchResultPage.editButton.isVisible());
    }

    @Test (dependsOnMethods = "SearchAnIssue")
    public void EditIssue() throws InterruptedException{
        logger.info("Existing Issue can be updated");
        SearchResultPage.editButton.click();
        EditIssuePage.EditIssue("This issue is updated");
        SearchResultPage.issueTitle.waitForExist(timeout);
        verifyEquals("This issue is updated", SearchResultPage.issueTitle.getText());
    }

}
