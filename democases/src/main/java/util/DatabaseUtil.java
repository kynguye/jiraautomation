package util;

import java.sql.*;


public class DatabaseUtil {

    public static String[][] getTableArray(String table) throws Exception
    {
        String myData [][] = null;

        try
        {
            int rowCount = 0;
            int columnCount;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://192.168.170.169:3306/autobots";
            Connection con = DriverManager.getConnection(url, "root", "autobots");

            // Execute the SQL statement
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * from " + table);

            // Get Column count
            ResultSetMetaData resultSet_metaData= resultSet.getMetaData();
            columnCount = resultSet_metaData.getColumnCount();

            // Get Row Count
            while( resultSet.next() )
                rowCount++;

            //Initialize data structure
            myData = new String [rowCount][columnCount];

            resultSet.beforeFirst();

            //populate data structure
            for(int row=0; row<rowCount; row++)
            {
                resultSet.next();
                for(int col=1; col<=columnCount; col++)
                    myData[row][col-1] = resultSet.getString(col);
            }

            stmt.close();
            con.close();
        }
        catch (Exception e){
            throw(e);
        }

        return myData;
    }
}
