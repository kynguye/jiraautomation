package util;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.InputStream;


public class ExcelUtil
{
    private static XSSFSheet ExcelWSheet;
    private static XSSFWorkbook ExcelWBook;
    private static XSSFCell Cell;
    private static XSSFRow Row;

    public static String[][] getTableArray(String FileName, String SheetName) throws Exception
    {
        String[][] tableArray = null;
        try
        {
            InputStream inputStream = ExcelUtil.class.getClassLoader().getResourceAsStream(FileName);
            ExcelWBook = new XSSFWorkbook(inputStream);
            ExcelWSheet = ExcelWBook.getSheet(SheetName);

            int startRow = 1;
            int startCol = 0;
            int ci,cj;
            int totalRows = ExcelWSheet.getLastRowNum();

            int totalCols = 2;
            tableArray=new String[totalRows][totalCols];
            ci=0;

            for (int i=startRow;i<=totalRows;i++, ci++)
            {
                cj=0;
                for (int j=startCol;j<totalCols;j++, cj++)
                {
                    tableArray[ci][cj]=getCellData(i,j);
                    System.out.println(tableArray[ci][cj]);
                }
            }
        }
        catch (Exception e){
            System.out.println("Could not find the file or sheet");
            throw(e);
        }

        return(tableArray);
    }

    public static String getCellData(int RowNum, int ColNum) throws Exception
    {
        try
        {
            return ExcelWSheet.getRow(RowNum).getCell(ColNum).getStringCellValue();
        }
        catch (Exception e){
            System.out.println("Could not find the file or sheet");
            throw(e);
        }
    }
}
