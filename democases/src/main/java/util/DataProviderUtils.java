package util;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DataProviderUtils {

    @Retention(RetentionPolicy.RUNTIME)
    public @interface Filter
    {
        String value();
    }

    public static String[][] FilterData(String[][] dataArray, Method method) throws Exception
    {
        try
        {
            List<String[]> finalList = new ArrayList<String[]>();

            // push all data from dataArray to finalList but the header row
            for (int i = 1; i < dataArray.length; i++) {
                finalList.add(dataArray[i]);
            }

            // if user use the Filter, then filter the data in finalList
            if (method.getAnnotation(Filter.class) != null)
            {
                String[] allFilters = method.getAnnotation(Filter.class).value().split(";");
                for (int filterRun = 0; filterRun < allFilters.length; filterRun++)
                {
                    String currentFilter = allFilters[filterRun];
                    String[] filterValue = currentFilter.split("=")[1].split(",");
                    Integer filterPosition = Arrays.asList(dataArray[0]).indexOf(currentFilter.split("=")[0]);

                    for (int i = 0; i < finalList.size(); i++) {
                        String[] item = finalList.get(i);
                        if (!Arrays.asList(filterValue).contains(item[filterPosition])) {
                            finalList.remove(i);
                        }
                    }
                }
            }

            // return the final data
            return finalList.toArray(new String[finalList.size()][]);
        }
        catch (Exception e)
        {
            throw (e);
        }
    }
}