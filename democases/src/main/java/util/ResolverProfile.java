package util;

import com.autobots.core.util.PropertiesUtil;
import org.springframework.test.context.ActiveProfilesResolver;


public class ResolverProfile implements ActiveProfilesResolver {

    @Override
    public String[] resolve(Class<?> aClass) {
        PropertiesUtil prop = new PropertiesUtil("config.properties");
        return new String[]{
                "common",
                prop.getProperty("platform")};
    }
}
