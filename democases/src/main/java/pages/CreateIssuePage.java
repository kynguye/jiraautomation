package pages;

import com.autobots.core.webcontrol.Button;
import com.autobots.core.webcontrol.ComboBox;
import com.autobots.core.webcontrol.Link;
import com.autobots.core.webcontrol.TextBox;

import javax.annotation.Resource;
import java.util.logging.Logger;

/**
 * Created by DellN4010 on 1/9/2015.
 */
public class CreateIssuePage extends HomePage{

    protected final static Logger logger = Logger.getLogger(CreateIssuePage.class.getName());

    public CreateIssuePage(){
        logger.info("Create Issue is created");
    }
    @Resource(name = "createLink" )
    public Link createLink;

    @Resource(name = "projectCombobox" )
    public ComboBox projectCombobox;

    @Resource(name = "issueTypeTextBox" )
    public TextBox issueTypeTextBox;

    @Resource(name = "summaryTextBox" )
    public TextBox summaryTextBox;

    @Resource(name = "assigneeTextBox" )
    public TextBox assigneeTextBox;

    @Resource(name = "createButton" )
    public Button createButton;

    @Resource(name = "issueCreated" )
    public Link issueCreated;

    private int timeout = 5000;

    public void SimpleCreateIssue(String summary)throws InterruptedException{
        createLink.waitForExist(timeout);
        createLink.click();
        summaryTextBox.enterText(summary);

        createButton.click();
    }

    public void CreateIssue(String project, String issue_type, String summary, String assignee){
        createLink.click();
//        projectCombobox.selectItem(project);
//        issueTypeTextBox.enterText(issue_type);
        summaryTextBox.enterText(summary);
//        assigneeTextBox.enterText(assignee);
        createButton.click();
    }
}
