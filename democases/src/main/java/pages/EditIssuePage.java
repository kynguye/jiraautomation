package pages;

import com.autobots.core.webcontrol.Button;
import com.autobots.core.webcontrol.ComboBox;
import com.autobots.core.webcontrol.Link;
import com.autobots.core.webcontrol.TextBox;

import javax.annotation.Resource;
import java.util.logging.Logger;

/**
 * Created by DellN4010 on 1/9/2015.
 */
public class EditIssuePage extends HomePage{

    protected final static Logger logger = Logger.getLogger(EditIssuePage.class.getName());

    public EditIssuePage(){
        logger.info("Edit Issue is created");
    }

    @Resource(name = "summaryTextBox" )
    public TextBox summaryTextBox;

    @Resource(name = "updateButton" )
    public Button updateButton;

    public void EditIssue(String summary){
        summaryTextBox.enterText(summary);
        updateButton.click();
    }

}
