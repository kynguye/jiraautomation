package pages;

import com.autobots.core.webcontrol.Button;
import com.autobots.core.webcontrol.Label;
import com.autobots.core.webcontrol.Link;
import com.autobots.core.webcontrol.TextBox;
import javax.annotation.Resource;
import javax.xml.soap.Text;
import java.util.logging.Logger;

/**
 * Created by DellN4010 on 1/6/2015.
 */
public class HomePage {
    protected final static Logger logger = Logger.getLogger(HomePage.class.getName());

    @Resource(name = "searchTextBox")
    public TextBox searchTextBox;

    public HomePage(){
        logger.info("Homepage is created");
      }
}
