package pages;

import com.autobots.core.webcontrol.Button;
import com.autobots.core.webcontrol.Label;
import com.autobots.core.webcontrol.TextBox;

import javax.annotation.Resource;
import java.util.logging.Logger;

/**
 * Created by DellN4010 on 1/9/2015.
 */
public class SearchResultPage extends HomePage {
    protected final static Logger logger = Logger.getLogger(SearchResultPage.class.getName());

    @Resource(name = "editButton" )
    public Button editButton;

    @Resource(name = "issueTitle" )
    public Label issueTitle;

    public SearchResultPage(){
        logger.info("Search Result is created");
    }
}
