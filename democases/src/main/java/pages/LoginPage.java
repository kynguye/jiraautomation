package pages;

import com.autobots.core.webcontrol.Button;
import com.autobots.core.webcontrol.Link;
import com.autobots.core.webcontrol.TextBox;

import javax.annotation.Resource;
import java.util.logging.Logger;

/**
 * Created by DellN4010 on 1/6/2015.
 */
public class LoginPage extends HomePage{
    protected final static Logger logger = Logger.getLogger(LoginPage.class.getName());

    public LoginPage(){
        logger.info("Login is created");
    }

    @Resource(name = "loginLink")
    public Link loginLink;

    @Resource(name ="emailAddress")
    public TextBox emailAddress;

    @Resource(name="passwordTextBox")
    public TextBox passwordTextBox;

    @Resource(name="loginButton")
    public Button loginButton;

    private int timeout = 5000;

    public void login(String emailaddress, String password)throws InterruptedException{
        loginLink.waitForExist(timeout);
        loginLink.click();
        emailAddress.enterText(emailaddress);
        passwordTextBox.enterText(password);
        loginButton.click();
    }
}
