package com.autobots.seleniumintegration.util;

import com.autobots.core.util.PropertiesUtil;
import org.openqa.selenium.Capabilities;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.autobots.core.webdriver.AutoWebDriver;
import com.autobots.seleniumintegration.webdriver.SeleniumDriverImpl;

import java.io.FileOutputStream;
import java.io.IOException;


public class SeleniumDriverFactory {
    private static final String PROFILES = "seleniumProfiles.xml";

    public synchronized static AutoWebDriver createDriverMatchWith(String propertiesFileName, String expectedBrowser, String platform) throws IOException {
        PropertiesUtil prop = new PropertiesUtil(propertiesFileName);
        if (expectedBrowser != null) {
            prop.setProperty("browser", expectedBrowser);
            prop.getProperties().store(new FileOutputStream(prop.getPropertiesFilePath().toFile()), null);
        }

        if (platform != null) {
            prop.setProperty("platform", platform);
            prop.getProperties().store(new FileOutputStream(prop.getPropertiesFilePath().toFile()), null);
        }

        ApplicationContext ctx = initApplicationContextMatchWith(prop, expectedBrowser, platform);
        AutoWebDriver driver;
        if (expectedBrowser == null) {
            driver = getDriverBean(prop.getProperty("browser"), ctx);
        } else {
            driver = getDriverBean(expectedBrowser, ctx);
        }

//        set implicit wait
        int implicitWaitSecond = 0;
        if (prop.getProperty("implicitWaitSecond")!= null) {
            implicitWaitSecond = Integer.parseInt(prop.getProperty("implicitWaitSecond"));
        }
        if (implicitWaitSecond >=0){
            driver.setImplicitWait(implicitWaitSecond);
        }

        return driver;
    }

    private static AutoWebDriver getDriverBean(String name, ApplicationContext ctx) {
        AutoWebDriver driver = (AutoWebDriver) ctx.getBean(name);
        ((SeleniumDriverImpl) driver).setCapabilities((Capabilities) ctx.getBean("cap"));
        return driver;
    }

    private static ApplicationContext initApplicationContextMatchWith(PropertiesUtil prop, String expectedBrowser, String platform) {
        SeleniumDriverFactory.setActiveProfiles(prop.getProperty("platform"), prop.getProperty("type"));
        System.setProperty("props.file", "file:" + prop.getPropertiesFilePath()); // set location of properties file
        return new ClassPathXmlApplicationContext(PROFILES);
    }

    private static void setActiveProfiles(String... name) {
        //profile "common" is always used;
        String activeProfiles = "common";
        for (String n : name) {
            activeProfiles += "," + n;
        }
        System.setProperty("spring.profiles.active", activeProfiles); // set active profile
    }

}
