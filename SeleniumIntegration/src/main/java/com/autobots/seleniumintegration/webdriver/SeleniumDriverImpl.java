package com.autobots.seleniumintegration.webdriver;

import com.autobots.core.webdriver.AutoWebDriver;
import com.autobots.core.webdriver.WebLocator;
import com.autobots.testng.util.ExceptionUtil;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;



public class SeleniumDriverImpl implements AutoWebDriver {
    protected WebDriver webDriver;
    protected Capabilities capabilities;

    public void TryToSleep(int milliseconds, boolean mobileOnly)
    {
        try
        {
            Capabilities cap = ((RemoteWebDriver) this.webDriver).getCapabilities();
            if (mobileOnly && !cap.getPlatform().toString().equals("WINDOWS"))
            {
                Thread.sleep(milliseconds);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public WebDriver getWebDriver(){
        return webDriver;
    }

    public SeleniumDriverImpl() {}

    public SeleniumDriverImpl(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    @Override
    public void closeAllBrowsers() {
        this.webDriver.quit();
    }

    @Override
    public void closeBrowser() {
        webDriver.quit();
        webDriver = this.cloneWebDriver();
    }

    @Override
    public void setImplicitWait(int timeout){
        webDriver.manage().timeouts().implicitlyWait(timeout,TimeUnit.SECONDS);
    }

    @Override
    public String getAlertText()
    {
        return webDriver.switchTo().alert().getText();
    }

    @Override
    public void confirmAlert(String button)
    {
        if (button.toLowerCase().equals("ok"))
        {
            webDriver.switchTo().alert().accept();
        }
        else
        {
            webDriver.switchTo().alert().dismiss();
        }
        TryToSleep(3000,true);
    }

    @Override
    public void clickElement(WebLocator locator) {
        loadControl(locator);
        this.findElement(locator).click();
        TryToSleep(3000,true);
    }

    @Override
    public void sendKey(WebLocator locator, String key) {
        loadControl(locator);
        this.findElement(locator).sendKeys(key);
        TryToSleep(3000,true);
    }

    @Override
    public void inputText(WebLocator locator, String text) {
        loadControl(locator);
        this.findElement(locator).sendKeys(text);
    }

    @Override
    public void clearText(WebLocator locator) {
        loadControl(locator);
        this.findElement(locator).clear();
    }

    @Override
    public String getText(WebLocator locator) {
        loadControl(locator);
        return findElement(locator).getText();
    }

    @Override
    public void selectItem(WebLocator locator, String item)
    {
        loadControl(locator);
        Select select = new Select(this.findElement(locator));
        select.selectByVisibleText(item);
    }

    @Override
    public String getSelectedItem(WebLocator locator)
    {
        loadControl(locator);
        Select select = new Select(this.findElement(locator));
        return select.getFirstSelectedOption().getText();
    }

    @Override
    public Integer getRowCount(WebLocator locator)
    {
        loadControl(locator);
        WebLocator row = new WebLocator(WebLocator.Type.XPATH,locator.getPath() + "//tr");
        return this.findElements(row).size();
    }

    @Override
    public boolean isElementVisible(WebLocator locator) {
        try {
            findElement(locator);
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    @Override
    public String getAttributeValue(WebLocator locator, String attr) {
        loadControl(locator);
        return this.findElement(locator).getAttribute(attr);
    }

    public Capabilities getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(Capabilities capabilities) {
        this.capabilities = capabilities;
    }

    @Override
    public void maximizeBrowserWindow() {
        try {
            this.webDriver.manage().window().maximize();
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
    }

    @Override
    public void openBrowser(String url) {
        this.webDriver.navigate().to(url);
    }

    private WebElement findElement(WebLocator locator) {
        WebElement element = null;
        WebLocator.Type type = locator.getType();
        switch (type) {
            case XPATH:
                element = this.webDriver.findElement(By.xpath(locator.getPath()));
                break;
            case CSS:
                element = this.webDriver.findElement(By.cssSelector(locator.getPath()));
                break;
        }
        return element;
    }

    private List<WebElement> findElements(WebLocator locator)
    {
        List<WebElement> elements = null;
        WebLocator.Type type = locator.getType();
        try {
            switch (type) {
                case XPATH:
                    elements = this.webDriver.findElements(By.xpath(locator.getPath()));
                    break;
                case CSS:
                    elements = this.webDriver.findElements(By.cssSelector(locator.getPath()));
                    break;
            }
        }
        catch(Throwable e)
        {
            ExceptionUtil.addVerificationFailure(e);
        }
        return elements;
    }

    private void loadControl(WebLocator webLocator){
        By by = null;
        WebLocator.Type type = webLocator.getType();
        switch (type) {
            case XPATH:
                by = By.xpath(webLocator.getPath());
                break;
            case CSS:
                by = By.cssSelector(webLocator.getPath());
                break;
        }
        // TODO: Replace 60 seconds by WaitTimeInShortSeconds in config file
        WebDriverWait wait = new WebDriverWait(webDriver, 60);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
        }
        catch(Throwable e)
        {
            ExceptionUtil.addVerificationFailure(e);
        }
    }

    protected WebDriver cloneWebDriver() {
        WebDriver clone = null;
        CommandExecutor executor = ((RemoteWebDriver) webDriver).getCommandExecutor();
        try {
            if (executor instanceof HttpCommandExecutor) {
                Constructor ctor = webDriver.getClass().getDeclaredConstructor(URL.class, Capabilities.class);
                URL remoteURL = ((HttpCommandExecutor) executor).getAddressOfRemoteServer();
                ctor.setAccessible(true);
                clone = (WebDriver)ctor.newInstance(remoteURL, getCapabilities());
            } else {
                Constructor ctor = webDriver.getClass().getDeclaredConstructor(Capabilities.class);
                ctor.setAccessible(true);
                clone = (WebDriver) ctor.newInstance(getCapabilities());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return clone;
    }

}
