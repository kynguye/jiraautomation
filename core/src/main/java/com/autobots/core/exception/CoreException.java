package com.autobots.core.exception;


public class CoreException extends Throwable {
    public CoreException(String message){
        super(message);
    }

}
