package com.autobots.core.webdriver;

public interface AutoWebDriver {

    /*********************
     * BROWSER REGION
     *********************/

    public void closeAllBrowsers();
    public void closeBrowser();
    public void maximizeBrowserWindow();
    public void openBrowser(String url);
    public void setImplicitWait(int timeout);
    public String getAlertText();
    public void confirmAlert(String button);

    /*********************
     * ELEMENT REGION
     *********************/
    public void clickElement(WebLocator locator);
    public void inputText(WebLocator locator, String text);
    public void clearText(WebLocator locator);
    public String getText(WebLocator locator);
    public void selectItem(WebLocator locator, String item);
    public String getSelectedItem(WebLocator locator);
    public boolean isElementVisible(WebLocator locator);
    public String getAttributeValue(WebLocator locator, String attr);
    public Integer getRowCount(WebLocator locator);
    public void sendKey(WebLocator locator, String key);

    /*********************
    * VERIFY REGION
    *********************/
}
