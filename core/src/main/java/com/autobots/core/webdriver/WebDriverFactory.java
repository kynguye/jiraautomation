package com.autobots.core.webdriver;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class WebDriverFactory {
    private static Map<Long, AutoWebDriver> driverMap = new ConcurrentHashMap<Long, AutoWebDriver>();

    public static void setAutoWebDriver(AutoWebDriver instance){
        driverMap.put(Thread.currentThread().getId(),instance);
    }

    public static AutoWebDriver getDriver(){
        return driverMap.get(Thread.currentThread().getId());
    }

}


