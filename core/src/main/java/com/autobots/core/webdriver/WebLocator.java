package com.autobots.core.webdriver;


public class WebLocator {
    private Type type;
    private String path;

    public WebLocator(Type type, String path) {
        this.setType(type);
        this.setPath(path);
    }

    public WebLocator(String path){
        this.setType(Type.XPATH);
        this.setPath(path);
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public enum Type {
        XPATH,
        CSS;
    }


}
