package com.autobots.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;


public class PropertiesUtil {
    private Properties prop;
    private String fileName;

    public PropertiesUtil(String fileName){
        this.fileName = fileName;
        prop = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
        try {
            prop.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void setProperty(String key, String value){
        prop.setProperty(key,value);
    }

    public String getProperty(String key){
        return prop.getProperty(key);
    }

    public Properties getProperties(){
        return prop;
    }

    public Path getPropertiesFilePath(){
        Path path = null;
        try{
            URL fileURL = new URL(getClass().getClassLoader().getResource(fileName).toString());
            path =  Paths.get(new URI(fileURL.toString()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return path;
    }


}
