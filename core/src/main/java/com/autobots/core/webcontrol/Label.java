package com.autobots.core.webcontrol;

import com.autobots.core.webdriver.WebDriverFactory;
import com.autobots.core.webdriver.WebLocator;


public class Label extends WebControl {

    public Label(WebLocator locator) {
        super(locator);
    }

    @Override
    public String getText() {
        return WebDriverFactory.getDriver().getText(getLocator());
    }
}
