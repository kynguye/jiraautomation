package com.autobots.core.webcontrol;

import com.autobots.core.webdriver.AutoWebDriver;
import com.autobots.core.webdriver.WebDriverFactory;
import com.autobots.core.webdriver.WebLocator;

public abstract class WebControl {
    private WebLocator locator;

    public WebControl(WebLocator locator) {
        this.setLocator(locator);
    }


    //-------------------------
    //COMMON METHODS
    //-------------------------

    public void click() {
        getDriver().clickElement(locator);
    }

    public void sendKey(String key) {
        throw new UnsupportedOperationException();
    }

    public void enterText(String text) {
        throw new UnsupportedOperationException();
    }

    public void clearText() {
        throw new UnsupportedOperationException();
    }

    public String getText() {
        throw new UnsupportedOperationException();
    }

    public boolean isVisible() {
        return getDriver().isElementVisible(locator);
    }

    public void waitForExist (int timeout) throws InterruptedException{
        int i = 0;
        boolean exist = false;
        while (i <= timeout && exist== false) {
            exist = isVisible();
            Thread.sleep(1000);
            i+=1;
        }
    }
    public String getAttributeValue(String name) {
        return getDriver().getAttributeValue(this.locator, name);
    }

    //---------------------------
    // GETTER & SETTER PROPERTIES
    //---------------------------
    public WebLocator getLocator() {
        return locator;
    }

    public void setLocator(WebLocator locator) {
        this.locator = locator;
    }

    protected AutoWebDriver getDriver() {
        return  WebDriverFactory.getDriver();
    }

 }
