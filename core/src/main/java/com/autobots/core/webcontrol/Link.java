package com.autobots.core.webcontrol;

import com.autobots.core.webdriver.WebDriverFactory;
import com.autobots.core.webdriver.WebLocator;


public class Link extends WebControl {
    public Link(WebLocator locator) {
        super(locator);
    }

    @Override
    public String getText() {
        return WebDriverFactory.getDriver().getText(getLocator());
    }
}
