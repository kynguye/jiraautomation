package com.autobots.core.webcontrol;

import com.autobots.core.webdriver.WebDriverFactory;
import com.autobots.core.webdriver.WebLocator;

public class ComboBox extends WebControl {

    public ComboBox(WebLocator locator) {
        super(locator);
    }

    public void selectItem(String item)
    {
        WebDriverFactory.getDriver().selectItem(getLocator(),item);
    }

    public String getSelectedItem()
    {
        return WebDriverFactory.getDriver().getSelectedItem(getLocator());
    }
}
