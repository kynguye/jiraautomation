package com.autobots.core.webcontrol;

import com.autobots.core.webdriver.WebDriverFactory;
import com.autobots.core.webdriver.WebLocator;


public class Table extends WebControl {

    public Table(WebLocator locator) {
        super(locator);
    }

    public Integer getRowCount()
    {
        return WebDriverFactory.getDriver().getRowCount(getLocator());
    }
}
