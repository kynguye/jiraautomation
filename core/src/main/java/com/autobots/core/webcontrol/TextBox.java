package com.autobots.core.webcontrol;

import com.autobots.core.webdriver.WebDriverFactory;
import com.autobots.core.webdriver.WebLocator;

public class TextBox extends WebControl {

    public TextBox(WebLocator locator) {
        super(locator);
    }

    @Override
    public void enterText(String text)
    {
        WebDriverFactory.getDriver().clearText(getLocator());
        WebDriverFactory.getDriver().inputText(getLocator(), text);
    }

    @Override
    public void clearText() {
        WebDriverFactory.getDriver().clearText(getLocator());
    }
}
