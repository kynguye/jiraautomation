package com.autobots.testng;

import com.autobots.testng.util.ExceptionUtil;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Listeners;

@Listeners({BaseTestListener.class})
public abstract class AbstractBaseTest extends AbstractTestNGSpringContextTests {



    public static void verifyTrue(boolean condition) {
        try {
            Assert.assertTrue(condition);
        } catch(Throwable e) {
            ExceptionUtil.addVerificationFailure(e);
        }
    }

    public static void verifyTrue(boolean condition, String message) {
        try {
            Assert.assertTrue(condition, message);
        } catch(Throwable e) {
            ExceptionUtil.addVerificationFailure(e);
        }
    }

    public static void verifyFalse(boolean condition) {
        try {
            Assert.assertFalse(condition);
        } catch(Throwable e) {
            ExceptionUtil.addVerificationFailure(e);
        }
    }

    public static void verifyFalse(boolean condition, String message) {
        try {
            Assert.assertFalse(condition, message);
        } catch(Throwable e) {
            ExceptionUtil.addVerificationFailure(e);
        }
    }

    public static void verifyEquals(boolean actual, boolean expected) {
        try {
            Assert.assertEquals(actual, expected);

        } catch(Throwable e) {
            ExceptionUtil.addVerificationFailure(e);
        }
    }

    public static void verifyEquals(Object actual, Object expected) {
        try {
            Assert.assertEquals(actual, expected);
        } catch(Throwable e) {
            ExceptionUtil.addVerificationFailure(e);
        }
    }

    public static void verifyEquals(Object actual, Object expected, int timeout) throws InterruptedException{
       int i = 0;
        while (i <=timeout && actual != expected) {
            try {
                Assert.assertEquals(actual, expected);
            } catch (Throwable e) {
                ExceptionUtil.addVerificationFailure(e);
            }
            Thread.sleep(1000);
            i+=1;
        }
    }

    public static void verifyEquals(Object[] actual, Object[] expected) {
        try {
            Assert.assertEquals(actual, expected);
        } catch(Throwable e) {
            ExceptionUtil.addVerificationFailure(e);
        }
    }

    public static void verifyEquals(Object actual, Object expected, String message) {
        try {
            Assert.assertEquals(actual, expected, message);
        } catch(Throwable e) {
            ExceptionUtil.addVerificationFailure(e);
        }
    }

    public static void fail(String message) {
        Assert.fail(message);
    }


}
