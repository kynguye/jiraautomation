package com.autobots.testng;

import com.autobots.core.exception.CoreException;
import com.autobots.testng.util.ExceptionUtil;
import org.testng.IInvokedMethod;
import org.testng.ITestResult;
import org.testng.Reporter;

import java.util.List;


public class BaseTestListener extends TestListenerAdapter {



    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult result) {

        Reporter.setCurrentTestResult(result);

        if (method.isTestMethod()) {

            List<Throwable> verificationFailures = ExceptionUtil.getVerificationFailures();

            if (verificationFailures.size() > 0) {

                //set the test to failed
                result.setStatus(ITestResult.FAILURE);

                //if there is an assertion failure add it to verificationFailures
                if (result.getThrowable() != null) {
                    verificationFailures.add(result.getThrowable());
                }

                int size = verificationFailures.size();

                if (size == 1) {
                    result.setThrowable(new CoreException(ExceptionUtil.customizeMessage(verificationFailures.get(0))));
                } else {
                    //create a failure message
                    StringBuffer failureMessage = new StringBuffer("\n\n===============================================\n");
                    failureMessage.append("<b>Multiple failures (").append(size).append("):</b>\n");
                    for (int i = 0; i < size-1; i++) {
                        failureMessage.append("-----------------------------------------------\n");
                        failureMessage.append("<b><i>Failure ").append(i+1).append(" of ").append(size).append(":</b></i>\n");
                        failureMessage.append(ExceptionUtil.customizeMessage(verificationFailures.get(i)));
                    }

                    //final failure
                    Throwable last = verificationFailures.get(size-1);
                    failureMessage.append("<b><i>Failure ").append(size).append(" of ").append(size).append(":</b></i>\n");
                    failureMessage.append(ExceptionUtil.customizeMessage(last));

                    //set merged throwable
                    //Throwable merged = new CoreException(failureMessage.toString());
                    //merged.setStackTrace(last.getStackTrace());

                    result.setThrowable(new CoreException(failureMessage.toString()));
                }

            }
        }
    }

}
