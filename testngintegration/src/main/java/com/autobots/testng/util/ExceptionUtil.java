package com.autobots.testng.util;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.internal.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ExceptionUtil {
    private static Map<ITestResult, List<Throwable>> verificationFailuresMap = new HashMap<ITestResult, List<Throwable>>();

    public static List<Throwable> getVerificationFailures() {
        List<Throwable> verificationFailures = verificationFailuresMap.get(Reporter.getCurrentTestResult());
        return verificationFailures == null ? new ArrayList<Throwable>() : verificationFailures;
    }

    public static void addVerificationFailure(Throwable e) {
        List<Throwable> verificationFailures = getVerificationFailures();
        verificationFailuresMap.put(Reporter.getCurrentTestResult(), verificationFailures);
        verificationFailures.add(e);
    }

    public static String customizeMessage(Throwable throwable){
        StringBuffer message = new StringBuffer();

        message.append("\n<i><u>Cause:</u></i> ").append(throwable.getMessage()).append("\n");
        int i = 0;
        for (StackTraceElement stackTraceElement : throwable.getStackTrace()){
            if (stackTraceElement.getClassName().contains("AbstractTestNGSpringContextTests")){
                break;
            }
            i+=1;
        }
        if (i >= 7) {
            message.append("<i><u>Where:</u></i> ").append(throwable.getStackTrace()[i - 7].toString()).append("\n");
        }
        else {
            message.append("<i><u>Where:</u></i> ").append(Utils.stackTrace(throwable, false)[1]).append("\n");
        }
        message.append("\n");
        return message.toString();
    }
}
