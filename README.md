autobots
========

## Remote Driver
#### Start Selenium Hub
Running the command

    java -jar selenium-server-standalone-2.44.0.jar -role hub

#### Start Selenium Node
Running the command

    java -jar selenium-server-standalone-2.44.0.jar -role node^
     -hub http://localhost:4444/grid/register^
     -Dwebdriver.ie.driver="D:\Workspace Java\transformers\SeleniumIntegration\src\main\resources\tools\IEDriverServer.exe"^
     -Dwebdriver.chrome.driver="D:\Workspace Java\transformers\SeleniumIntegration\src\main\resources\tools\chromedriver.exe"
 
  
  
### TODO:
How to run on multi platforms, types, browsers. E.g.

    Run on android & windows - local & remote - chrome & firefox

